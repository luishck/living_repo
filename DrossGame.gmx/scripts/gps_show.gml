///gps_show(lugar1,lugar2,lugar3,lugar4,lugar5)
//crear menú
if !instance_exists(cuadro_gps) instance_create(obj_dross.x,obj_dross.y,cuadro_gps);
//if instance_exists(obj_dir)

if argument0 = "Biblioteca"{
        with(instance_create(cuadro_gps.x,cuadro_gps.y-80,boton_minim)){
         text = argument0;
         goto = teleport_biblio;
        }
        }

if argument1 = "Casa"{
    with(instance_create(cuadro_gps.x,cuadro_gps.y-48,boton_minim)){
         text = argument1;
         goto = teleport_casa_dross;
    }
}

if argument2 != ""{
    with(instance_create(cuadro_gps.x,cuadro_gps.y-16,boton_minim)){
             text = argument2;
             goto = teleport_biblio;
    }
}

if argument3 != ""{
    with(instance_create(cuadro_gps.x,cuadro_gps.y+16,boton_minim)){
             text = argument3;
             goto = teleport_biblio;
    }
}

if argument4 != ""{
    with(instance_create(cuadro_gps.x,cuadro_gps.y+48,boton_minim)){
             text = argument4;
             goto = teleport_biblio;
    }
}
