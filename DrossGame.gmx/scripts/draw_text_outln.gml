var _x, _y, outline, precision, text, dir_inc, dir, dist_inc, dist;
_x = argument0;
_y = argument1;
outline = argument2;
text = argument3;
color= argument4
dprecision = 8;
lprecision = 2;
dir_inc = 360 / dprecision;
dist_inc = outline / lprecision;
draw_set_color(c_black);
for (dir = 0; dir < 360; dir += dir_inc)
{
for (dist = dist_inc; dist <= outline; dist += dist_inc)
{
draw_text_ext(_x+lengthdir_x(dist,dir),_y+lengthdir_y(dist,dir),text,30,650);
}
}
draw_set_color(color);
draw_text_ext(_x,_y,text,30,650);
