var r1,r2,g1,g2,b1,b2;
r1 = argument0
g1 = argument1
b1 = argument2
r2 = argument3
b2 = argument4
g2 = argument5

return (difference(r1,r2) + difference(b1,b2) + difference(g1,g2))/3