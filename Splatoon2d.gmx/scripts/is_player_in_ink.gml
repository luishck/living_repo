var topleft,topright,bottomleft,bottomright;

topleft = abgr_get_rgb(sampler_sample(global.playersampler,0,0))
topright = abgr_get_rgb(sampler_sample(global.playersampler,0,10*controller.quality))
bottomleft = abgr_get_rgb(sampler_sample(global.playersampler,10*controller.quality,0))
bottomright = abgr_get_rgb(sampler_sample(global.playersampler,10*controller.quality,10*controller.quality))

if !((abs(color_get_red(topleft)-color_get_blue(global.color1)) < 10
and abs(color_get_green(topleft)-color_get_green(global.color1)) < 10
and abs(color_get_blue(topleft)-color_get_red(global.color1)) < 10)
or (abs(color_get_red(topright)-color_get_blue(global.color1)) < 10
and abs(color_get_green(topright)-color_get_green(global.color1)) < 10
and abs(color_get_blue(topright)-color_get_red(global.color1)) < 10)
or (abs(color_get_red(bottomleft)-color_get_blue(global.color1)) < 10
and abs(color_get_green(bottomleft)-color_get_green(global.color1)) < 10
and abs(color_get_blue(bottomleft)-color_get_red(global.color1)) < 10)
or (abs(color_get_red(bottomright)-color_get_blue(global.color1)) < 10
and abs(color_get_green(bottomright)-color_get_green(global.color1)) < 10
and abs(color_get_blue(bottomright)-color_get_red(global.color1)) < 10))
{

return true
}
/*
if in_ink = true
{
if show_question(string(color_get_red(topleft)) + " : " + string(color_get_blue(global.color1)) + "#" +
string(color_get_green(topleft)) + " : " + string(color_get_green(global.color1)) + "#" +
string(color_get_blue(topleft)) + " : " + string(color_get_red(global.color1))) = false
{
game_end()
}
}*/