///Disables buttons and stuff
if show_question("Do you want the game to ignore any joysticks that continually return a value higher than the deadzone?") = true
{
joe = 0
repeat(11)
{
global.disable[joe] = 0
joe+=1
}
show_message("Set the controller down and click OK")
for (h = 0; h < 11 ; h+=1)
{
if string(check_controller(1,.1)) != "-1"
{
global.disable[h] = check_controller(1,.1)
//show_message("Disabled" + global.disable[h])
}
}
}