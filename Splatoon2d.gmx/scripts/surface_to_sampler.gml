///surface_to_sampler(ind)
//Get a sampler from a surface.
var sampler,w,h,tbuff,surfey;
sampler=ds_map_create()

if !surface_exists(argument0)
{
surfy = surface_create(10,10)
}
else
{
surfy = argument0
}

w = surface_get_width(surfy)
h = surface_get_height(surfy)
ds_map_add(sampler,"width",w)
ds_map_add(sampler,"height",h)
tbuff=buffer_create(w * h * 4,buffer_fixed,4)
buffer_get_surface(tbuff,surfy,0,0,0)
ds_map_add(sampler,"data",tbuff)
global.last = -1
global.lastw = -1
global.lastb = -1
return sampler;