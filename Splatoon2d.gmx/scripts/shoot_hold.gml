if !in_ink
{

if weapon = "splattershot"
{
if ink >= 2
{

if !audio_is_playing(my_shot_long)
{
my_shot_long = audio_play_sound(shot_long,10,0)
}

repeat(1)
{
bullet = instance_create(x+cosine(image_angle+90)*15,y-sine(image_angle+90)*15,bullet_object2)
bullet.direction = image_angle + random(16)-8
bullet.speed = 15
bullet.time_out = 300*abs(player2.trigger_joy)/bullet.speed

if bullet.time_out > 20
{
bullet.time_out = 20
}
}
ink-=2
}
else
{
if should_play = true
{
if !audio_is_playing(no_ink_sound)
{
audio_play_sound(no_ink_sound,10,0)
}
should_play = false
}
}

}

if weapon = "chargeshot"
{

if charge < 5
{
//audio_play_sound(charge_start,10,0)
mycharging = audio_play_sound(charge_charging,10,0)
}
if (charge+60)/1.5 < ink
{
charge+= 6
}
else
{
if should_play = true
{
if !audio_is_playing(no_ink_sound)
{
audio_play_sound(no_ink_sound,10,0)
}
should_play = false
}

if audio_is_playing(mycharging)
{
audio_stop_sound(mycharging)
}

}


if charge > 399
{
charge = 399
}

}

if weapon = "splattershotjr"
{

if ink >= 2.2
{


if !audio_is_playing(my_shot_short)
{
my_shot_short = audio_play_sound(shot_short,10,0)
}


repeat(2)
{
bullet = instance_create(x+cosine(image_angle+90)*15,y-sine(image_angle+90)*15,bullet_object2)
bullet.direction = image_angle + random(24)-12
bullet.speed = 20
bullet.time_out = 180*abs(player2.trigger_joy)/bullet.speed
if bullet.time_out > 9
{
bullet.time_out = 9
}
}
ink -= 2.2
}
else
{
if should_play = true
{
if !audio_is_playing(no_ink_sound)
{
audio_play_sound(no_ink_sound,10,0)
}
should_play = false
}
}

}

if weapon = "roller"
{
timergo -= 1
if timergo < 0 and ink  >= 2 and is_moving
{

if !audio_is_playing(my_roller_sound) 
{
my_roller_sound = audio_play_sound(roller_sound,10,1)
}

bullet = instance_create(x,y,bullet_roller_object2)
bullet.speed = 65
bullet.time_out = 1
bullet.direction = image_angle+50

bullet = instance_create(x,y,bullet_roller_object2)
bullet.speed = 47
bullet.time_out = 1
bullet.direction = image_angle+30

bullet = instance_create(x,y,bullet_roller_object2)
bullet.speed = 40
bullet.time_out = 1
bullet.direction = image_angle

bullet = instance_create(x,y,bullet_roller_object2)
bullet.speed = 47
bullet.time_out = 1
bullet.direction = image_angle-30

bullet = instance_create(x,y,bullet_roller_object2)
bullet.speed = 65
bullet.time_out = 1
bullet.direction = image_angle-50

ink -= 2
}
else
{
if audio_is_playing(my_roller_sound)
{
audio_stop_sound(my_roller_sound)
}
}

if ink < 2
{
if should_play = true
{
if !audio_is_playing(no_ink_sound)
{
audio_play_sound(no_ink_sound,10,0)
}
should_play = false
}
}

}


if !is_moving
{
if audio_is_playing(my_roller_sound)
{
audio_stop_sound(my_roller_sound)
}
}

}