//check_controller(id,deadzone)
var buttons;
buttons = joystick_buttons(argument0);
for (i = 0; i<buttons; i+=1)
{
if joystick_check_button(argument0,i)
{
return i;
}
}


if joystick_xpos(argument0) < -argument1
{
return "left";
}

if joystick_xpos(argument0) > argument1
{
return "right";
}



if joystick_ypos(argument0) < -argument1
{
return "up";
}
if joystick_ypos(argument0) > argument1
{
return "down";
}


if joystick_zpos(argument0) > argument1
{
return "twistleft";
}
if joystick_zpos(argument0) < -argument1
{
return "twistright";
}


if joystick_rpos(argument0) < -argument1
{
return "left2";
}
if joystick_rpos(argument0) > argument1
{
return "right2";
}


if joystick_upos(argument0) < -argument1
{
return "up2";
}
if joystick_upos(argument0) > argument1
{
return "down2";
}


if joystick_vpos(argument0) < -argument1
{
return "twistleft2";
}
if joystick_vpos(argument0) > argument1
{
return "twistright2";
}


if joystick_pov(argument0) != -1
{
return string(joystick_pov(argument0));
}


return "-1";