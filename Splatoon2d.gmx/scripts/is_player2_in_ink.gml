/*var topleft,topright,bottomleft,bottomright;

topleft = abgr_get_rgb(sampler_sample(global.player2sampler,0,0))
topright = abgr_get_rgb(sampler_sample(global.player2sampler,0,10))
bottomleft = abgr_get_rgb(sampler_sample(global.player2sampler,10,0))
bottomright = abgr_get_rgb(sampler_sample(global.player2sampler,10,10))

if (color_get_red(topleft) >= color_get_red(c_orange) or color_get_red(topright) >= color_get_red(c_orange) or color_get_red(bottomleft) >= color_get_red(c_orange) or color_get_red(bottomright) >= color_get_red(c_orange))
{
return true;
}
*/

var topleft,topright,bottomleft,bottomright;

topleft = abgr_get_rgb(sampler_sample(global.player2sampler,0,0))
topright = abgr_get_rgb(sampler_sample(global.player2sampler,0,10*controller.quality))
bottomleft = abgr_get_rgb(sampler_sample(global.player2sampler,10*controller.quality,0))
bottomright = abgr_get_rgb(sampler_sample(global.player2sampler,10*controller.quality,10*controller.quality))

if !((abs(color_get_blue(topleft)-color_get_red(global.color2)) < 10
and abs(color_get_red(topleft)-color_get_blue(global.color2)) < 10
and abs(color_get_green(topleft)-color_get_green(global.color2)) < 10)
or (abs(color_get_blue(topright)-color_get_red(global.color2)) < 10
and abs(color_get_red(topright)-color_get_blue(global.color2)) < 10
and abs(color_get_green(topright)-color_get_green(global.color2)) < 10)
or (abs(color_get_blue(bottomleft)-color_get_red(global.color2)) < 10
and abs(color_get_red(bottomleft)-color_get_blue(global.color2)) < 10
and abs(color_get_green(bottomleft)-color_get_green(global.color2)) < 10)
or (abs(color_get_blue(bottomright)-color_get_red(global.color2)) < 10
and abs(color_get_red(bottomright)-color_get_blue(global.color2)) < 10
and abs(color_get_green(bottomright)-color_get_green(global.color2)) < 10))
{
return true
}