if keyboard_check(ord('S'))
{
if !position_meeting(x+cosine(image_angle+180)*maxspeed*3,y,wall) and !position_meeting(x+cosine(image_angle+180)*maxspeed*3,y,wall_p1)
{
x += cosine(image_angle+180)*maxspeed
}

if !position_meeting(x,y-sine(image_angle+180)*maxspeed*3,wall) and !position_meeting(x,y-sine(image_angle+180)*maxspeed*3,wall_p1)
{
y -= sine(image_angle+180)*maxspeed
}

rollerframe -= .3
}


if keyboard_check(ord('A')) and ! keyboard_check(ord('S')) and !keyboard_check(ord('W'))
{
if !position_meeting(x+cosine(image_angle+90)*maxspeed*3,y,wall) and !position_meeting(x+cosine(image_angle+90)*maxspeed*3,y,wall_p1)
{
x += cosine(image_angle+90)*maxspeed
}

if !position_meeting(x,y-sine(image_angle+90)*maxspeed*3,wall) and !position_meeting(x,y-sine(image_angle+90)*maxspeed*3,wall_p1)
{
y -= sine(image_angle+90)*maxspeed
}

rollerframe -= .3
}


if keyboard_check(ord('D')) and ! keyboard_check(ord('S')) and !keyboard_check(ord('W'))
{
if !position_meeting(x+cosine(image_angle-90)*maxspeed*3,y,wall) and !position_meeting(x+cosine(image_angle-90)*maxspeed*3,y,wall_p1)
{
x += cosine(image_angle-90)*maxspeed
}

if !position_meeting(x,y-sine(image_angle-90)*maxspeed*3,wall) and !position_meeting(x,y-sine(image_angle-90)*maxspeed*3,wall_p1)
{
y -= sine(image_angle-90)*maxspeed
}

rollerframe -= .3
}



if keyboard_check(ord('W'))
{
if !position_meeting(x+cosine(image_angle)*maxspeed*3,y,wall) and !position_meeting(x+cosine(image_angle)*maxspeed*3,y,wall_p1)
{
x += cosine(image_angle)*maxspeed
}
if !position_meeting(x,y-sine(image_angle)*maxspeed*3,wall) and !position_meeting(x,y-sine(image_angle)*maxspeed*3,wall_p1) 
{
y -= sine(image_angle)*maxspeed
}
rollerframe += .3
}