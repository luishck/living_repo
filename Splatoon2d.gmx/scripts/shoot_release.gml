should_play = true

if !in_ink
{

if weapon = "chargeshot"
{
ii = 0
if charge > 340
{
charge = 340
}

if (charge+60)/1.5 > ink
{
charge = ink*1.5
}

if audio_is_playing(mycharging)
{
audio_stop_sound(mycharging)
}

if charge > 300
{
audio_play_sound(charge_shot,1,0)
}
else
{
audio_play_sound(charge_shot_soft,1,0)
}
ink -= (charge+60)/1.5
repeat((charge+60)/5)
{
/*
bullet = instance_create(x+cos(degtorad(image_angle))*(ii*8)+(random(30)-15),y-sin(degtorad(image_angle))*(ii*8)+(random(30)-15),blob_object)
bullet = instance_create(x+cos(degtorad(image_angle))*(ii*8)+(random(30)-15),y-sin(degtorad(image_angle))*(ii*8)+(random(30)-15),blob_object)
*/
bullet = instance_create(x+cosine(image_angle+90)*10,y-sine(image_angle+90)*10,bullet_sure_object2)
bullet.speed = 25+random(5)
bullet.time_out= (ii*8)/bullet.speed
bullet.direction = image_angle+ random(2)
bullet = instance_create(x+cosine(image_angle+90)*10,y-sine(image_angle+90)*10,bullet_sure_object2)
bullet.speed = 25+random(5)
bullet.direction = image_angle- random(2)
bullet.time_out= (ii*8)/bullet.speed
bullet = instance_create(x+cosine(image_angle+90)*10,y-sine(image_angle+90)*10,bullet_sure_object2)
bullet.speed = 25+random(5)
bullet.direction = image_angle
bullet.time_out= (ii*8)/bullet.speed
bullet = instance_create(x+cosine(image_angle+90)*(5+random(10)),y-sine(image_angle+90)*(5+random(10)),bullet_sure_object2)
bullet.speed = 25+random(5)
bullet.direction = image_angle
bullet.time_out= (ii*8)/bullet.speed

ii += 1
}
charge = 0

}

}

if audio_is_playing(my_roller_sound)
{
audio_stop_sound(my_roller_sound)
}