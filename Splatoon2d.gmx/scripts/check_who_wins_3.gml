texture_set_interpolation(true)
//gg
game_call_surface = surface_create(room_width/10,room_height/10)
surface_set_target(game_call_surface)
draw_surface_ext(splat_surface,0,0,.1,.1,0,c_white,1)
surface_reset_target()

game_call_sampler = surface_to_sampler(game_call_surface)

colly = get_sampler_color_3(game_call_sampler)

if colly = true
{
global.winner = "player3win"
}
else
{
global.winner = "player1win"
}