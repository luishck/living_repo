var ii,jj,full_color,ccc,jojo, player1score, player2score;
afull_color = 0;
ii = 0;
jj = 0;
ccc = 0
player1score = 0;
player2score = 0;

repeat(sampler_get_height(argument0))
{
repeat(sampler_get_width(argument0))
{
ccc = abgr_get_rgb(sampler_sample(argument0,ii,jj))
ccb = color_get_red(ccc)
ccg = color_get_green(ccc)
ccr = color_get_blue(ccc)

if ((abs(ccr-color_get_red(global.color1)) < 10
and abs(ccg-color_get_green(global.color1)) < 10
and abs(ccb-color_get_blue(global.color1)) < 10))
{
player1score += 1
}

if ((abs(ccr-color_get_red(global.color2)) < 10
and abs(ccg-color_get_green(global.color2)) < 10
and abs(ccb-color_get_blue(global.color2)) < 10))
{
player2score += 1
}

ii += 1
}
jj += 1
ii = 0
}

global.p1percent = player1score/(sampler_get_height(argument0)*sampler_get_width(argument0))
global.p2percent = player2score/(sampler_get_height(argument0)*sampler_get_width(argument0))

if player2score >= player1score
{
return true; //P2 Wins
}
else
{
return false; //P1 Wins
}