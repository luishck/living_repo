///controller_press(id,deadzone,button/joystick) = Boolean
//if controller_press(1,.25,right) = true...
if string(joystick_pov(argument0)) = string(argument2) or string(joystick_pov(argument0)) = string(realernine(argument2)+45) or string(joystick_pov(argument0)) = string(realernine(argument2)-45)
or string(joystick_pov(argument0)) = string(realernine(argument2)+360-45)

{
return true;
}


if joystick_check_button(argument0,realer(argument2))
{
return true;
}

if string(argument2) = "twist"
{
if abs(joystick_zpos(argument0)) > argument1
{
return joystick_zpos(argument0);
}
else
{
return 0;
}
}



if string(argument2) = "verticle"
{
if abs(joystick_ypos(argument0)) > argument1
{
return joystick_ypos(argument0);
}
else
{
return 0;
}
}

if string(argument2) = "horizontal"
{
if abs(joystick_xpos(argument0)) > argument1
{
return joystick_xpos(argument0);
}
else
{
return 0;
}
}

if string(argument2) = "verticle2"
{
if abs(joystick_rpos(argument0)) > argument1
{
return joystick_rpos(argument0);
}
else
{
return 0;
}
}

if string(argument2) = "horizontal2"
{
if abs(joystick_upos(argument0)) > argument1
{
return joystick_upos(argument0);
}
else
{
return 0;
}
}




if string(argument2) = "left"
if joystick_xpos(argument0) < -argument1
{
return true;
}
if string(argument2) = "right"
if joystick_xpos(argument0) > argument1
{
return true;
}
if string(argument2) = "up"
if joystick_ypos(argument0) < -argument1
{
return true;
}
if string(argument2) = "down"
if joystick_ypos(argument0) > argument1
{
return true;
}
if string(argument2) = "twistright"
if joystick_zpos(argument0) < -argument1
{
return true;
}
if string(argument2) = "twistleft"
if joystick_zpos(argument0) > argument1
{
return true;
}
if string(argument2) = "left2"
if joystick_rpos(argument0) < -argument1
{
return true;
}
if string(argument2) = "right2"
if joystick_rpos(argument0) > argument1
{
return true;
}
if string(argument2) = "down2"
if joystick_upos(argument0) > argument1
{
return true;
}
if string(argument2) = "up2"
if joystick_upos(argument0) < -argument1
{
return true;
}
if string(argument2) = "twistleft2"
if joystick_vpos(argument0) < -argument1
{
return true;
}
if string(argument2) = "twistright2"
if joystick_vpos(argument0) > argument1
{
return true;
}

return false;